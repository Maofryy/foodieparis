""" Scraping yelp fusion api for paris restaurants """
import os
import json
import sys
import tqdm
from yelp_utils import YelpFusion

# ----- Testing Yelp fuison API endpoint ----- #
def data_scraper(res_path):
    """Retrieving restaurants data by zipcode (no overlapping of data possible)"""
    api_key = '-SuRS0lQOboO4Axbdj9Zk66yeLUIM9_a4kVr3XrDeYFzUouF6ZSFRXfpWHUGUm3jDMS17FRaLTvi1UVoWlWeR0LlA21baXyGzBIpUIXoExwedNWMFpW_lawTUtXgXnYx'
    api = YelpFusion(api_key)
    if api.test_endpoint() != 200:
        print("bad status code received from url")
        sys.exit()

    # ----- Getting yelp restaurants zipcodes ----- #
    restaurants_in_paris = []
    restaurants_zip = []

    # Calling our Api class and retrieving data by zipcode #
    #Iterating through zip codes and filtering items with zip and cedex codes (no duplicates possible, and sorting by distance to get all items)
    for ardt in tqdm.tqdm(range(1, 21), ncols=80, desc='ZIP codes'):
        # Cycle through restaurants
        zip_code = "750"+str(ardt).zfill(2)
        zip_cedex = "751"+str(ardt).zfill(2)
        #
        restaurants_zip = (api.search_places_by_zipcode(zip_code+", Paris"))
        #Filter by 750XX and 751XX
        restaurants_in_paris.extend([x for x in restaurants_zip if x['location']['zip_code'] == zip_code or x['location']['zip_code'] == zip_cedex])

    print(str(len(restaurants_in_paris))+" restaurants queried.")


    # ----- Handling and saving the data ----- #
    # Dump the unique restaurants in paris_restaurants.json
    os.makedirs(os.path.dirname(res_path), exist_ok=True)
    restaurants_file = open(res_path, 'w+')
    json.dump(restaurants_in_paris, restaurants_file, indent=6)
    restaurants_file.close()

    # Free memory space
    restaurants_in_paris = None

if __name__ == "__main__":
    my_path = os.path.dirname(os.path.abspath(__file__))
    data_scraper(my_path + "\\..\\data\\yelp\\paris_restaurants.json")
