""""Contains helping functions, more related to pandas and api handle than on data analysis"""
import time
import requests
import numpy as np
import pandas as pd
from tqdm import tqdm

class YelpFusion():
    """Class interface with the yelp fusion api
    """

    def __init__(self, api_key):
        super(YelpFusion, self).__init__()
        self.api_key = api_key
        self.headers = {'Authorization': 'bearer %s' % self.api_key}

    def search_places_by_zipcode(self, zip_code):
        """Calling api to get all/most of restaurants according to the zipcode provided

        Args:
            zip_code (string): 5 digits zip code strin

        Returns:
            json: containing all the query done through yelp
        """

        endpoint = "https://api.yelp.com/v3/businesses/search"
        places = []
        parameters = {
            'term': 'restaurants',
            'offset': 0,
            'limit': 50,
            'location' : zip_code,
            'sort_by' : 'distance'
        }
        for offset_number in tqdm(range(0, 1000, 50), ncols=80, desc=zip_code):
            parameters['offset'] = offset_number
            response = requests.get(url=endpoint, params=parameters, headers=self.headers)
            if not response.json().get('businesses', False):
                break
            places.extend(response.json()['businesses'])
            time.sleep(0.5) ## Don't want to get blocked by Yelp API
        return places

    def test_endpoint(self):
        """Testing the status code of api's endpoint we intend to request from

        Returns:
            int: status code from the endpoint request
        """

        endpoint = "https://api.yelp.com/v3/businesses/search"
        parameters = {
            'term': 'restaurants',
            'location': 'Paris'
        }
        response = requests.get(url=endpoint, params=parameters, headers=self.headers)
        return response.status_code





def expand_list(dfm, lst_col):
    """function expanding the dataframe passed with new columns of each field of the column containing multi dimensional objects
    A little different from normalize function but with the same goal/principle

    Args:
        dfm (pandas.Dataframe): dataframe to work on
        lst_col (string): label of column to expand

    Returns:
        pandas.Dataframe: new dataframe with new columns
    """

    dfm = pd.DataFrame({
        col:np.repeat(dfm[col].values, dfm[lst_col].str.len())
        for col in dfm.columns.drop(lst_col)}).assign(**{lst_col:np.concatenate(dfm[lst_col].values)})[dfm.columns]
    return dfm




def bayes_avg(dfm, cond, col):
    """Function computing the bayes average of a datafram column

    Args:
        dfm (pandas.Dataframe): dataframe to evaluate on
        cond (string): label describing condition to groupby
        col (string): label of the column to average

    Returns:
        pandas.Series: series with bayes average according to the conditon
    """

    val = dfm.groupby(cond)[col].size()
    mean = dfm[cond].value_counts().mean()
    weight = val/(val+mean)
    score = weight*dfm.groupby(cond)[col].mean() + (1-weight)*dfm[col].mean()
    return score

def zip_to_location(zip_code):
    """Translate paris zipcode to their cedex relative : 750XX => 751XX

    Args:
        zip_code (string): 5 digit zipcode

    Returns:
        string: corresponding cedex code
    """

    zip_to_loc_dict = {
        "75001": 75101,
        "75002": 75102,
        "75003": 75103,
        "75004": 75104,
        "75005": 75105,
        "75006": 75106,
        "75007": 75107,
        "75008": 75108,
        "75009": 75109,
        "75010": 75110,
        "75011": 75111,
        "75012": 75112,
        "75013": 75113,
        "75014": 75114,
        "75015": 75115,
        "75016": 75116,
        "75017": 75117,
        "75018": 75118,
        "75019": 75119,
        "75020": 75120}
    return zip_to_loc_dict.get(zip_code[:5], zip_code[:5])


def extract_values(obj, key):
    """Pull all values of specified key from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, val in obj.items():
                if isinstance(val, (dict, list)):
                    extract(val, arr, key)
                elif k == key:
                    arr.append(val)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results
