"""Testing files for pipeline"""
from foodieparis.foodie.yelp_utils import YelpFusion

def test_api_status():
    """Testing Yelp fusion api response status code
    """
    api_key = '-SuRS0lQOboO4Axbdj9Zk66yeLUIM9_a4kVr3XrDeYFzUouF6ZSFRXfpWHUGUm3jDMS17FRaLTvi1UVoWlWeR0LlA21baXyGzBIpUIXoExwedNWMFpW_lawTUtXgXnYx'
    api = YelpFusion(api_key)
    assert api.test_endpoint() == 200
