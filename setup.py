from distutils.core import setup

setup(
    name='FoodieParis',
    version='1.2',
    description='A exploratory analysis of Paris restaurants with Yelp api',
    author='Maori Benhassine',
    author_email='maori.benhassine@gmail.com',
    packages=['foodie',],
    url='https://gitlab.com/Maofryy/foodieparis',
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
    data_files=[('ressources data', ['data'])],
)
