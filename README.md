# FoodieParis: An exploratory data analysis on Paris, France restaurants data scraped from Yelp

An exploratory analysis of paris restaurants using

* Yelp fusion and Google Places api to query about restaurants prices, rating and reviews

* Cleaning the data in pre-processing

* Show statistics with 3 axes (Summary, categorial and geographical)

## Table of Contents

* [Background](#background)
* [Findings](#findings)
  * [Summary Statistics](#summary-statistics)
  * [Categorical Statistics](#categorical-statistics)
  * [Geographic Statistics](#geographic-statistics)
* [Usage](#usage)
* [Legality](#legality)

## Background

In my search of an apartment in Paris, i asked myself is some "arrondissement" where better rated than others in terms au restaudrants and food services
So an exploratory analysis of the biggest places reviewing app/websites was the perfect case study to learn more about geographical analysis

* Is french cuisine receiving good reviews from locals and visitors ?

* On average, where are the highest rated restaurants located ?

* Which category of cuisine is the most common to find and which is the better rated ?

In this exploratory data analysis, I found out answers to these questions and more with my trusty friend: data.

[Here](#answers) I answer to theses questions

## Findings

### Summary Statistics

<img src="foodieparis/data/pics/Summary.png" width="1500" ><br/>
Here are the general summary statistics for restaurants in Paris, France, according to Yelp data. As we can see in the above graphs, most restaurants have less than 100 reviews, most are within a rating of 3.0 - 4.5, and majority are given a double money sign in terms of price.

### Categorical Statistics

<img src="foodieparis/data/pics/Cat_rating_reviewcount.png" width="1300" ><br/>
<img src="foodieparis/data/pics/Cat_price.png" width="1300" >
Next, we will look at categorical data of the same properties. The graphs above show the better rated and reviewed categories as well as the difference of price between them. Below showcase the best 5 categories of theses parameters. This data suggests that most categories are pretty close in terms of average price, rating, and review count. We have many french and italian restaurants in Paris.

<img src="foodieparis/data/pics/Best5_1.png" width="700" ><br/>
<img src="foodieparis/data/pics/Best5_2.png" width="1000" ><br/>

### Geographic Statistics

Lastly, we have data for the areas of Paris, per "arrondissements" for average rating, restaurant density, average price, and review count.

**Summary data by zip_codes**
<img src="foodieparis/data/pics/Zip_rating_reviewcount.png" width="1000" ><br/>
<img src="foodieparis/data/pics/Zip_price.png" width="1000" ><br/>

**Average Rating**
<img src="foodieparis/data/pics/map_area_rating.png" width="1000" ><br/>
It seems that the average ratings are distributed throughout Paris, with the 11th arrondissement being the best and 8th the worst area to go for food.

**Restaurant Density**
<img src="foodieparis/data/pics/map_restaurants.png" width="1000" ><br/>
It's no surprise that the upper bank of Paris has the most restaurants, with the numbers decreasing as the city becomes more residential.

**Average Price**
<img src="foodieparis/data/pics/map_area_price.png" width="1000" ><br/>

With almost no correlation to average rating, the average price is really representative of the rich ardts of Paris, we can clearly see this disparity on the Center/West side of the city.

**Review Count**
<img src="foodieparis/data/pics/map_area_reviewcount.png" width="1000" ><br/>
It's pretty surprising to see that the area with the highest restaurant reviews are located more in the western side of Vancouver, given that the highest density of restaurants is downtown. The average ratings for that area are not particularly high, either. A possibility is that the western side of Vancouver has a particularly committed customer base, or Yelp is popular within that community.

## Answers

To the questions I may have asked myself here is what the data found :

*Is french cuisine receiving good reviews from locals and visitors ?*
> In average rating french restaurants are 74/270 (top 27.41%) so in the upper third but far from the better rated categories

*On average, where are the highest rated restaurants located ?*
> There is a cleary distinction from the **Average** rating of the 11th, 3rd and 15th arrondissements.

*Which genre of cuisine is the most common to find and which is the better rated ?*
> The most common is of course french cuisine but the better rated are vegan and deli restaurants

## Usage

[Click to open map in browser](https://maofryy.gitlab.io/foodieparis/)

Upon collecting data with the data_yelp module, the programs cleans and process it into a html leaflet map that can be opened in any browser, then it is possible to select the different layers of data and to visualize it sorted and averaged by zip code

**Example**
<img src="foodieparis\data\pics\map_japanese_sushisbars.png" width="1000"><br/>
Here I selected **japanese** and **sushi_bars** to display the averaging rating of theses categories of restaurants across Paris arrondissements, the average difference is very small but they are still some better than others.

## Legality

This is a personal project made for non-commercial uses ONLY. This project will not be used to generate any promotional or monetary value for me, the creator, or the user.

*If you are a Yelp representative and there are any legal issues with this project, please contact me!*

This project uses the Yelp Fusion API, and terms and conditions are found here:
<https://www.yelp.ca/developers/api_terms>

"Allowable non-commercial use of the Yelp Content. Notwithstanding the foregoing, you may use the Yelp Content to perform certain analysis for non-commercial uses only, such as creating rich visualizations or exploring trends and correlations over time, so long as the underlying Yelp Content is only displayed in the aggregate as an analytical output, and not individually. For example, this is an acceptable non-commercial analytical use of the Yelp Content. “Non-commercial use” means any use of the Yelp Content which does not generate promotional or monetary value for the creator or the user, or such use does not gain economic value from the use of our content for the creator or user, i.e. you. If you are interested in commercial use of the Yelp Content, please visit www.yelp.com/knowledge."

### Trello Link

<https://trello.com/b/pRiSix5g/foodieparis>

### Gitlab Link

<https://gitlab.com/Maofryy/foodieparis>
