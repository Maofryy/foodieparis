""""Loading, cleaning and analysing data gathered from yelp"""
import os
import json
import warnings
import folium
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from folium.plugins import FastMarkerCluster
from pandas.io.json import json_normalize
from yelp_utils import bayes_avg
from yelp_utils import expand_list
from yelp_utils import zip_to_location
from data_yelp import data_scraper

warnings.filterwarnings('ignore')
plt.style.use('seaborn')
sns.set(palette='cubehelix')
plt.rcParams['font.monospace'] = 'Andale mono'

# ----- Test if data_yelp need to be run ------- #

my_path = os.path.dirname(os.path.abspath(__file__))
if not os.path.isfile(my_path + '\\..\\data\\yelp\\paris_restaurants.json'):
    print("Running data scraper :")
    data_scraper(my_path + "\\..\\data\\yelp\\paris_restaurants.json")

# ----- Loading restaurants and categories ----- #
with open(my_path + '\\..\\data\\yelp\\paris_restaurants.json') as f:
    data = json.load(f)

#Loading in dataframe
df = pd.DataFrame.from_dict(data, orient='columns')

# ----- Cleaning data ----- #
#Remove unnecessary data
df = df.drop(columns=['image_url', 'is_closed', 'transactions', 'phone', 'display_phone'])
df.replace(['€', '€€', '€€€', '€€€€'], [1.0, 2.0, 3.0, 4.0], inplace=True)

dfm = df.copy()

#Summary stats
fig, axes = plt.subplots(1, 3, figsize=(20, 5))
sns.set_palette(sns.cubehelix_palette(8))
axes[0].set_xscale('log')
axes[0].set(ylim=(0, 0.015))
for ax in axes:
    ax.grid(False)
sns.distplot(df['review_count'].fillna(0), hist=False, color="g", ax=axes[0],
             kde_kws={"shade": True}, bins=np.arange(0.000, 0.015, 0.0005)).set_title('review_count')
sns.countplot(df['rating'], ax=axes[1]).set_title('rating')
sns.countplot(df['price'], ax=axes[2]).set_title('price')
fig.suptitle('Summary distributions')

#plt.show()

# -------------- PLOT BY CATEGORIES ------- #
### Pre processing data

#One-hot encode categories list
df_c = df.copy()

df_categories = df_c[['id', 'categories']]
df_c.drop(columns=['categories'], inplace=True)
df_c.rename(columns={'alias':'r_alias'}, inplace=True)

df_expanded = df_c.copy().merge(expand_list(df_categories.copy(), 'categories'))
df_categories = expand_list(df_categories, 'categories')
df_categories = pd.concat([df_categories.drop(['categories'], axis=1),
                           df_categories['categories'].apply(pd.Series)], axis=1)
df_categories = df_categories.groupby(['id']).agg(lambda x: tuple(x)).applymap(list).reset_index()

df_c = df_c.merge(df_categories, on='id')
df_c = df_c.drop('alias', 1).join(df_c.alias.str.join('|').str.get_dummies())

df_expanded = pd.concat([df_expanded.drop(['categories'], axis=1),
                         df_expanded['categories'].apply(pd.Series)], axis=1)

df_expanded.rename(columns={'alias':'category'}, inplace=True)

#Displaying the first n items
N = 30

### PRICE ###
pr = (bayes_avg(df_expanded.dropna(how='any'), 'category', 'price').sort_values(ascending=False)
      .reset_index().sort_values('price', ascending=False))
high_pr = pr.head(int(N/3))
low_pr = pr.tail(int(N/3)).iloc[::-1]
### RATING
ra = (bayes_avg(df_expanded, 'category', 'rating').sort_values(ascending=False)
      .reset_index().sort_values('rating', ascending=False))
# At which position is french cuisine rated on yelp ?
print("At which position is french cuisine rated on yelp ?")
fr_index = ra[ra['category'] == 'french'].index.values.astype(int)[0]
total_index = ra['category'].count()
print(str(fr_index)+"/"+str(total_index)+" | top "+str(round(fr_index/total_index*100, 2))+"%")
ra = ra.head(N)


### REVIEW_COUNT
re = (bayes_avg(df_expanded, 'category', 'review_count').sort_values(ascending=False)
      .reset_index().sort_values('review_count', ascending=False))
re = re.head(N)
### NUMBER OF RESTAURANTS

nu = (df_c.drop(['id', 'r_alias', 'name', 'url', 'review_count', 'rating', 'coordinates',
                 'price', 'location', 'distance', 'title'], axis=1).sum().sort_values(ascending=False))
nu = nu.reset_index().head(N)
nu.rename(columns={'index': 'category', 0:'total_num'}, inplace=True)




# first fig
fig, axes = plt.subplots(1, 2, figsize=(20, 7))
sns.set_palette(sns.cubehelix_palette(8))
for ax in axes:
    ax.grid(False)
sns.barplot(data=high_pr, x='price', y='category', palette="PuRd_r", orient='h', ax=axes[0]) \
    .set_title('Highest Average Price by Category', fontsize=15)
axes[0].set_ylabel('')
axes[0].set(xlim=(2.3, 2.7))
axes[0].set_xlabel('')

sns.barplot(data=low_pr, x='price', y='category', palette="PuRd", orient='h', ax=axes[1]) \
    .set_title('Lowest Average Price by Category', fontsize=15)
axes[1].set_ylabel('')
axes[1].set(xlim=(1.7, 2.1))
axes[1].set_xlabel('')

# second fig
fig, axes = plt.subplots(1, 2, figsize=(15, 8))
sns.set_palette('ch:8')

for ax in axes:
    ax.grid(False)
sns.barplot(data=ra, x='rating', y='category', orient='h', ax=axes[0], palette="BuGn_r").set_title('Average Rating by Category', fontsize=12)
axes[0].set_ylabel('')
axes[0].set(xlim=(3.8, 4.1))

sns.barplot(data=re, x='review_count', y='category', orient='h', ax=axes[1], palette="OrRd_r") \
    .set_title('Average review Count by Category', fontsize=12)
axes[1].set_ylabel('')
axes[1].set(xlim=(20, 50))

plt.tight_layout()
#plt.show()

# let's map the top 5 of each category
M = 5

high_pr, low_pr, ra, re, nu = high_pr.head(M), low_pr.head(M), ra.head(M), re.head(M), nu.head(M)

#first fig
fig, axes = plt.subplots(1, 2, figsize=(20, 6))
sns.set_palette(sns.cubehelix_palette(8))
for ax in axes:
    ax.grid(False)
sns.barplot(data=ra, x='rating', y='category', orient='h', ax=axes[0], palette="BuGn_r").set_title('5 Best by Average Rating', fontsize=20)
axes[0].set_ylabel('')
sns.barplot(data=high_pr, x='price', y='category', orient='h', ax=axes[1], palette="PuRd_r").set_title('5 Best by Average Price', fontsize=20)
axes[1].set_ylabel('')




# second fig
fig, axes = plt.subplots(1, 2, figsize=(20, 6))
sns.set_palette('ch:8')
for ax in axes:
    ax.grid(False)
sns.barplot(data=re, x='review_count', y='category', orient='h', ax=axes[0], palette="OrRd_r") \
    .set_title('5 Best by Review Count', fontsize=20)
axes[0].set_ylabel('')

sns.barplot(data=nu, x='total_num', y='category', orient='h', ax=axes[1], palette="OrRd_d") \
    .set_title('5 Best by Total Number', fontsize=20)
axes[1].set_ylabel('')

plt.tight_layout()
#plt.show()

# -------------- PLOT BY ZIPCODES ------- #

### Pre processing data
#One hot encode location list
df_location = df[['id', 'location']]

df.drop(columns=['location'], inplace=True)

df_zip = df.copy().merge(df_location.copy())

df_location = pd.concat([df_location.drop(['location'], axis=1), df_location['location'].apply(pd.Series)], axis=1)
df_location = df_location.groupby(['id']).agg(lambda x: tuple(x)).applymap(list).reset_index()

df = df.merge(df_location, on='id')
df = df.drop('zip_code', 1).join(df.zip_code.str.join('|').str.get_dummies())

df_zip = pd.concat([df_zip.drop(['location'], axis=1), df_zip['location'].apply(pd.Series)], axis=1)

#One-hot encode by categories
df_categories = df_zip[['id', 'categories']]
df_zip.drop(columns=['categories'], inplace=True)
df_zip.rename(columns={'alias':'r_alias'}, inplace=True)

df_flat = df_zip.copy().merge(expand_list(df_categories.copy(), 'categories'))
df_categories = expand_list(df_categories, 'categories')
df_categories = pd.concat([df_categories.drop(['categories'], axis=1), df_categories['categories'].apply(pd.Series)], axis=1)
df_categories = df_categories.groupby(['id']).agg(lambda x: tuple(x)).applymap(list).reset_index()

df_zip = df_zip.merge(df_categories, on='id')
df_zip = df_zip.drop('alias', 1).join(df_zip.alias.str.join('|').str.get_dummies())

df_flat = pd.concat([df_flat.drop(['categories'], axis=1), df_flat['categories'].apply(pd.Series)], axis=1)

df_flat.rename(columns={'alias':'category'}, inplace=True)


#Displaying the first n items
N = 10

### PRICE ###
pr = (bayes_avg(df_zip.dropna(how='any'), 'zip_code', 'price').sort_values(ascending=False).reset_index().sort_values('price', ascending=False))
high_pr = pr.iloc[:N]
low_pr = pr.tail(N).iloc[::-1]
N = 15
### RATING
ra = (bayes_avg(df_zip, 'zip_code', 'rating').sort_values(ascending=False).reset_index().sort_values('rating', ascending=False))
ra = ra.iloc[:N]
### REVIEW_COUNT
re = (bayes_avg(df_zip, 'zip_code', 'review_count').reset_index().sort_values('review_count', ascending=False).reset_index(drop=True))

re = re.iloc[:N]
### NUMBER OF RESTAURANTS
nu = (df.drop(['id', 'alias', 'name', 'url', 'review_count', 'categories', 'rating',
               'coordinates', 'price', 'distance', 'address1', 'address2', 'address3',
               'city', 'country', 'state', 'display_address'], axis=1).sum().sort_values(ascending=False))
nu = nu.reset_index(name='values').sort_values('values', ascending=False).iloc[:N]
nu.rename(columns={'index': 'zip_code', 'values':'total_num'}, inplace=True)


# first fig
fig, axes = plt.subplots(1, 2, figsize=(20, 7))
sns.set_palette(sns.cubehelix_palette(8))
for ax in axes:
    ax.grid(False)

sns.barplot(data=high_pr, x='price', y='zip_code', orient='h', ax=axes[0], order=high_pr.zip_code, palette="PuRd_r") \
    .set_title('Highest Average Price by zip code', fontsize=15)
axes[0].set_ylabel('')
axes[0].set(xlim=(2.1, 2.5))

sns.barplot(data=low_pr, x='price', y='zip_code', orient='h', ax=axes[1], order=low_pr.zip_code, palette="PuRd") \
    .set_title('Lowest Average Price by zip code', fontsize=17)
axes[1].set_ylabel('')
axes[1].set(xlim=(2.0, 2.3))

# second fig
fig, axes = plt.subplots(1, 2, figsize=(15, 7))
sns.set_palette('ch:8')
for ax in axes:
    ax.grid(False)

sns.barplot(data=ra, x='rating', y='zip_code', orient='h', ax=axes[0], order=ra.zip_code, palette="BuGn_r") \
    .set_title('Average Rating by zip code', fontsize=13)
axes[0].set_ylabel('')
axes[0].set(xlim=(3.4, 3.9))

sns.barplot(data=re, x='review_count', y='zip_code', orient='h', ax=axes[1], order=re.zip_code, \
            palette="OrRd_r").set_title('Average review Count by zip code', fontsize=13)
axes[1].set_ylabel('')
axes[1].set(xlim=(15, 27))

plt.tight_layout()
#plt.show()

# ------------ PARIS ARDT MAP ------------- #
#Add neighbourhood name based on zip code (see Geographic Data)

print("---------------- Working with folium -------------------------")

with open(my_path + '/../data/paris/arrondissements.geojson', 'r') as f:
    geojson = json.load(f)

# ------- Adding all restaurants as markers ---- #

paris_map = folium.Map(location=[48.8566, 2.3522], zoom_start=13)



df_markers = json_normalize(data)[['coordinates.latitude', 'coordinates.longitude']]
df_markers.dropna(inplace=True)

FastMarkerCluster(data=list(zip(df_markers['coordinates.latitude'].values, df_markers['coordinates.longitude'].values)),
                  name='Restaurants', show=True).add_to(paris_map)

###   Add density of restaurants ###
df_flat['arinsee'] = df_flat['zip_code'].apply(zip_to_location)
de = df_flat.groupby(['arinsee']).size().reset_index()

lay = folium.Choropleth(geo_data=geojson, data=de, columns=['arinsee', 0], key_on='feature.properties.c_arinsee',
                        fill_opacity=0.7, fill_color='OrRd', legend_name="Number", name='Restaurant density', show=True)
#pylint: disable=protected-access
for key in lay._children:
    if key.startswith('color_map'):
        del lay._children[key]
#pylint: enable=protected-access
lay.add_to(paris_map)

### Add average price of restaurants ###
pr = (bayes_avg(df_flat.dropna(how='any'), 'arinsee', 'price').sort_values(ascending=False).reset_index().sort_values('price', ascending=False))
folium.Choropleth(geo_data=geojson, data=pr, columns=['arinsee', 'price'], key_on="feature.properties.c_arinsee",
                  fill_color='PuRd', fill_opacity=0.7, line_opacity=0.5, smooth_factor=1,
                  nan_fill_color='black', nan_fill_opacity=0.2, legend_name="Average Price", name='Area price', show=False).add_to(paris_map)

###   Add average rating of restaurants ###
ra = (bayes_avg(df_flat, 'arinsee', 'rating').sort_values(ascending=False).reset_index().sort_values('rating', ascending=False))
folium.Choropleth(geo_data=geojson, data=ra, columns=['arinsee', 'rating'], key_on="feature.properties.c_arinsee",
                  fill_color='BuGn', fill_opacity=0.7, line_opacity=0.5, smooth_factor=1,
                  nan_fill_color='black', nan_fill_opacity=0.2, legend_name="Average Rating", name='Area rating', show=False).add_to(paris_map)

### Add review count of restaurants
rc = df_flat[['arinsee', 'review_count']].groupby(['arinsee']).sum().reset_index()
folium.Choropleth(geo_data=geojson, data=rc, columns=['arinsee', 'review_count'], key_on="feature.properties.c_arinsee",
                  fill_color='OrRd', fill_opacity=0.7, line_opacity=0.5, smooth_factor=1,
                  nan_fill_color='black', nan_fill_opacity=0.2, legend_name="reviews count", name='Review count', show=False).add_to(paris_map)


#Add an average rating of each category by ardt
#For each category
df_flat = df_flat.iloc[df_flat.groupby('category').category.transform('size').mul(-1).argsort(kind='mergesort')]
df_catsorted = df_flat['category'].unique()
for cat in df_catsorted[:35]:
    # Add new choropeth with df_flat of corresponding catrbash
    ra = (bayes_avg(df_flat[df_flat['category'] == cat], 'arinsee', 'rating').sort_values(ascending=False)
          .reset_index().sort_values('rating', ascending=False))
    lay = folium.Choropleth(geo_data=geojson, data=ra, columns=['arinsee', 'rating'], key_on="feature.properties.c_arinsee",
                            fill_color='GnBu', fill_opacity=0.6, line_opacity=0.5, smooth_factor=1, bins=np.arange(3.0, 4.75, 0.25),
                            nan_fill_color='black', nan_fill_opacity=0.2, legend_name="Category average Rating",
                            name=str(df_flat[df_flat['category'] == cat].iloc[0]['title'])+' ', show=False)
    #pylint: disable=protected-access
    if cat != "french":
        for key in lay._children:
            if key.startswith('color_map'):
                del lay._children[key]
    #pylint: enable=protected-access
    lay.add_to(paris_map)

folium.LayerControl().add_to(paris_map)

#Save map file to be used in gitlab pages
os.makedirs(os.path.dirname(my_path + '/../../public/index.html'), exist_ok=True)
paris_map.save(my_path + '/../../public/index.html')
